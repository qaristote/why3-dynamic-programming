
open Format
open Dynamic

(**  {2 Tests for Longest Increasing Sequence} *)

let pp_array fmt a =
  fprintf fmt "@[<hov 3>[| ";
  for i=0 to Array.length a - 1 do
    fprintf fmt "%a;@ " Z.pp_print a.(i)
  done;
  fprintf fmt "|]@]"

let run msg g a =
  let t = Unix.gettimeofday () in
  let x = g a in
  let t = Unix.gettimeofday () -. t in
  printf "@[<hov 2>[%2.2f] %s on an array of length %d:@ %a ->@ %a@]@."
    t msg (Array.length a) pp_array a Z.pp_print x

let run_array msg g a =
  let t = Unix.gettimeofday () in
  let x = g a in
  let t = Unix.gettimeofday () -. t in
  printf "@[<hv 2>[%2.2f] %s on an array of length %d:@ %a ->@ %a@]@."
    t msg (Array.length a) pp_array a pp_array x


(** example sequence given in the project text *)
let test1 =
  Array.map Z.of_int
    [| 8; 4; 12; 1; 11; 6; 14; 15; 10; 5; 13; 3 |]

(* pseudo-random arrays as tests *)
let () = Random.init 2022

let random_array n =
  Array.init n (fun _ -> Z.of_int (Random.int 2022))

let test2 = random_array 20

let test3 = random_array 40

let test4 = random_array 60

let test5 = random_array 80

let test6 = random_array 100

let test7 = random_array 120

let () =
  run "lis" lis test1;
  run_array "all_lis_ending_at" all_lis_ending_at test1;
  run_array "lis_dynamic" lis_dynamic test1;
  run "lis" lis test2;
  run_array "all_lis_ending_at" all_lis_ending_at test2;
  run_array "lis_dynamic" lis_dynamic test2;
  run "lis" lis test3;
  run_array "all_lis_ending_at" all_lis_ending_at test3;
  run_array "lis_dynamic" lis_dynamic test3;
  run "lis" lis test4;
  run_array "all_lis_ending_at" all_lis_ending_at test4;
  run_array "lis_dynamic" lis_dynamic test4;
  run "lis" lis test5;
  run_array "all_lis_ending_at" all_lis_ending_at test5;
  run_array "lis_dynamic" lis_dynamic test5;
  run "lis" lis test6;
  run_array "all_lis_ending_at" all_lis_ending_at test6;
  run_array "lis_dynamic" lis_dynamic test6;
  run "lis" lis test7;
  run_array "all_lis_ending_at" all_lis_ending_at test7;
  run_array "lis_dynamic" lis_dynamic test7;
  ()
