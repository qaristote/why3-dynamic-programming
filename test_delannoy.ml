
open Format
open Dynamic

(**  {2 Tests for Delannoy numbers} *)


let run msg g n m =
  let t = Unix.gettimeofday () in
  let x = g n m in
  let t = Unix.gettimeofday () -. t in
  printf "@[[%2.2f] %s(%d,%d): %a@]@." t msg n m Z.pp_print x

let () =
  let f n m = Dynamic.d (Z.of_int n) (Z.of_int m) in
  run "delannoy" f 0 0;
  run "delannoy" f 1 1;
  run "delannoy" f 2 2;
  run "delannoy" f 3 3;
  run "delannoy" f 4 4;
  run "delannoy" f 5 5;
  run "delannoy" f 6 6;
  run "delannoy" f 7 7;
  run "delannoy" f 8 8;
  run "delannoy" f 9 9;
  run "delannoy" f 10 10;
  run "delannoy" f 11 11;
  (* comment below if takes too long *)
  run "delannoy" f 12 12;
  run "delannoy" f 13 13;
  ()

let () =
  let f n m = Dynamic.delannoy_matrix_alt (Z.of_int n) (Z.of_int m) in
  run "delannoy_matrix_alt" f 0 0;
  run "delannoy_matrix_alt" f 1 1;
  run "delannoy_matrix_alt" f 2 2;
  run "delannoy_matrix_alt" f 5 5;
  run "delannoy_matrix_alt" f 10 10;
  run "delannoy_matrix_alt" f 20 20;
  run "delannoy_matrix_alt" f 50 50;
  run "delannoy_matrix_alt" f 100 100;
  run "delannoy_matrix_alt" f 200 200;
  run "delannoy_matrix_alt" f 500 500;
  run "delannoy_matrix_alt" f 1000 1000;
  run "delannoy_matrix_alt" f 2000 2000;
  run "delannoy_matrix_alt" f 3000 3000;
  run "delannoy_matrix_alt" f 4000 4000;
  (* comment below if takes too long or runs out of memory *)
  run "delannoy_matrix_alt" f 5000 5000;
  run "delannoy_matrix_alt" f 7000 7000;
  ()

let () =
  let f n m = Dynamic.delannoy_dynamic (Z.of_int n) (Z.of_int m) in
  run "delannoy_dynamic" f 10 10;
  run "delannoy_dynamic" f 50 50;
  run "delannoy_dynamic" f 100 100;
  run "delannoy_dynamic" f 500 500;
  run "delannoy_dynamic" f 1000 1000;
  run "delannoy_dynamic" f 2000 2000;
  run "delannoy_dynamic" f 3000 3000;
  run "delannoy_dynamic" f 4000 4000;
  (* comment below if takes too long *)
  run "delannoy_dynamic" f 5000 5000;
  run "delannoy_dynamic" f 7000 7000;
  run "delannoy_dynamic" f 10000 10000;
  ()
