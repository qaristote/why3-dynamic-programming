.PHONY: test_delannoy test_lis 

default:
	@echo 'type `make <target>` where <target> is either ide, test_delannoy, test_lis, replay or doc'

ide:
	why3 ide dynamic.mlw

test_delannoy: dynamic.mlw
	why3 extract -D ocaml64 dynamic.mlw -o dynamic.ml
	ocamlbuild -pkg unix -pkg zarith test_delannoy.native
	./test_delannoy.native

test_lis: dynamic.mlw
	why3 extract -D ocaml64 dynamic.mlw -o dynamic.ml
	ocamlbuild -pkg unix -pkg zarith test_lis.native
	./test_lis.native

replay:
	why3 replay dynamic

doc:
	why3 doc dynamic.mlw
	why3 session html dynamic

clean:
	rm -rf _build
